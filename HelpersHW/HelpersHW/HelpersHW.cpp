﻿#include <iostream>
#include "Helpers.h"

int main()
{
    float a;
    float b;

    std::cout << "Enter the first number: ";
    std::cin >> a;

    std::cout << "Enter the second number: ";
    std::cin >> b;

    std::cout << "Square of sum: " << sqrSum(a, b);
}